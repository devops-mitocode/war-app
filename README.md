
# Microservicio Jenkins

Se realizo un Jenkinsfile que cumple con las siguientes caracteristicas.



## Descripción

 - Utilizar una aplicación que disponga hecha en Maven que genere un artefacto de
tipo WAR..
 - Crear un repositorio Gitlab público y alojar la aplicación.
 - Crear un pipeline(Jenkinsfile) con los siguientes stages :
    
    - Build
    - Testing(Junit + Jacoco)
    - Sonar (cobertura mayor a 60%)
    - Nexus
    - Despliegue en Jboss(Ansible)  [URL JBOOS](http://18.205.156.140:8080/war-app-1.0.4/devp)


## Feedback

Si tiene un comentario se puede cominicar a  epadilla@apilink.com.mx


## 🚀 Autor
- [@eduardo-padilla-cruz](https://www.linkedin.com/in/eduardo-padilla-cruz)