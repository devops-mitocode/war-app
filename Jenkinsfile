def war_file = ""
pipeline {
    agent any
    stages {

        stage('Compile') {
            agent {
                docker 'maven:3.6.3-jdk-11'
            }
            steps {
                sh 'mvn clean compile -B -ntp'
            }
        }
          stage('Test') {
            agent {
                docker 'maven:3.6.3-jdk-11'
            }

            steps {
                sh 'mvn test -B -ntp'
            }
            post {
                always {
                    echo 'always'
                }
                success {
                    echo 'success'
                   
                    junit 'target/surefire-reports/*.xml'

                    jacoco()
                }
                failure {
                    echo 'failure'
                }
            }
        }
           stage('Build') {
             agent {
                docker 'maven:3.6.3-jdk-11'
            }
            steps {
                sh 'mvn package -DskipTests -B -ntp'
            }
        }
          stage('Sonarqube') {
              agent {
                docker 'maven:3.6.3-jdk-11'
            }
            steps {
                withCredentials([file(credentialsId: 'sonarqube-settings', variable: 'M2_SETTINGS')]) {
                    sh "mvn sonar:sonar -B -ntp -s ${M2_SETTINGS}"
                }
                
            }
        }
         stage('Nexus') {
              agent {
                docker 'maven:3.6.3-jdk-11'
            }
            steps {
                script {

                    def pom = readMavenPom file: 'pom.xml'

                    
                    war_file="${pom.artifactId}-${pom.version}.war"
                    println (war_file)
                    nexusPublisher nexusInstanceId: 'nexus', 
                    nexusRepositoryId: 'war-app-releases', 
                    packages: [[
                        $class: 'MavenPackage', 
                        mavenAssetList: [[classifier: '', extension: '', filePath: "target/${pom.artifactId}-${pom.version}.war"]],
                        mavenCoordinate: [artifactId: "${pom.artifactId}", groupId: "${pom.groupId}", packaging: 'war', version: "${pom.version}"]]
                    ]
                }
            }
        }
    
        stage('Deploy with Ansible') {
            agent {
                docker {
                    image 'quay.io/ansible/ansible-runner:stable-2.12-latest'
                    args '-u root'
                }
            }
            environment {
                ANSIBLE_HOST_KEY_CHECKING = 'False'
            }
            steps {
                sshagent(credentials: ['epadilla-aws-linux']) {
                   dir('ansible'){
                        sh 'ansible-galaxy --version'

                        sh 'ansible-galaxy collection install community.general'

                        sh "ansible-playbook --extra-vars war=$war_file -i hosts deployJBoss.yml -vvv"
                   }
                }
            }
        }
    }

   
}
